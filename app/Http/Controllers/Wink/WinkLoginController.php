<?php

namespace App\Http\Controllers\Wink;

use App\Http\Controllers\Controller;

class WinkLoginController extends Controller
{
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('wink.login');
    }
}
