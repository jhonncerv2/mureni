@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card shadow-lg">
                    <div class="card-header d-flex flex-column align-items-center">
                        <img src="/img/mureni-logo.png" class="card-logo" alt="mureni logo">
                        <h1 class="card-brand">{{ __('mureni') }}</h1>
                    </div>

                    <div class="card-body">
                        <form class="login" method="POST" action="{{ route('wink.auth.attempt') }}">
                            @csrf

                            <div class="form-group row justify-content-center">
                                <div class="col-md-10">
                                    <input id="email" type="email" class="form-control login__email @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="col-md-10">
                                    <input id="password" type="password" class="form-control login__password @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="col-md-10">
                                    <button type="submit" class="btn login__button">
                                        {{ __('Ingresar') }}
                                    </button>
                                </div>

                                <div class="col-md-10">
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link login__link login__link--mbotom" href="{{ route('password.request') }}">
                                            {{ __('¿Necesitas ayuda?') }}
                                        </a>
                                    @endif
                                </div>

                                <div class="col-md-10">
                                    <a class="btn btn-link login__link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
